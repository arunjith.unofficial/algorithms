function linearSearch(array, x) {
    let foundAt=-1
    for (let i=0; i<array.length ; i++) {
        if (array[i]===x) {
            foundAt=i;
        }
        
    }
    if (foundAt===-1) {
        console.log("not found");
    }
    else{
        console.log("found at",foundAt)
    }
}
// linearSearch([1,2,3,4,5,6],3)

function binarySearch(array, x){
    let start = 0;
    let end = array.length;
    let count=0;
    let foundAt=-1
    while(start<=end && count<60){
        let middle = (start + end) /2;
        count++;
        
        if (x=== array[Math.ceil(middle)]) {
            foundAt=Math.ceil(middle);
            return foundAt
        }
        else if (x<array[Math.ceil(middle)]){
            end=Math.ceil(middle)-1
        }
        else if (x>array[Math.ceil(middle)]){
            start=Math.ceil(middle)+1
        }
    }

}  
let arr = [];  
for(let i = 0; i<100000000; i++){
    arr.push(i)
}

let x= prompt('enter a number:');
let startTime= Date.now()
const result=binarySearch (arr,x);
let endTime= Date.now()
let timeTaken = (endTime - startTime)/1000;
console.log(`Completed binary search in ${timeTaken} seconds`)

console.log("Searching with linear search")
startTime = Date.now()
const resultLS=linearSearch (arr,x)
endTime = Date.now()
timeTaken = (endTime - startTime)/1000;
console.log(`Completed linear search in ${timeTaken} seconds`)

if (result===-1) {
    console.log("not found")
}
else {
    console.log(result)
}
