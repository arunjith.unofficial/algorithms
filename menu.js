console.log("Welcome to the Melange");

function displayMenu(){
    console.log("1) Vegetarian\n1) Non vegetarian\n3) Starters\n4) Desserts")
}

function giveFeedback(){
    const feedback = prompt("Enter your feedback");
    console.log(feedback)
}

function makePayment(){
    console.log("Payment made")
}

let exit = false;
while(exit === false){
    const optionSelected = prompt("Select a menu option \n")
    switch(optionSelected){
        case '1': 
            displayMenu();
            break;
        case '2': 
            giveFeedback();
            break;
        case '3': 
            makePayment();
            break;
        case '4': exit = true; break;
        default: console.log("Invalid entry\n")
    }
}